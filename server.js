var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get("/", function(req, res){
   //res.send("Hola Mundo!");
   res.sendFile(path.join(__dirname, 'index.html'));
 })

 app.get("/clientes/:idcliente", function(req, res){
  res.send("Aqui tienes el cliente numero: " +req.params.idcliente);
 })

//app.post("/", function(req, res){
//  res.send("Hemos recibido su peticiòn");
//})

app.post("/", function(req, res){
  res.send("Hemos recibido su peticon actualizada")

})

app.put("/", function(req, res){
  res.send("Hemos actualizado su peticiòn");
})

app.delete("/", function(req, res){
  res.send("Hemos borrado su peticiòn");
})
